(function ($) {

/**
 * Attaches sticky table headers.
 */
Drupal.behaviors.formFooter = {
  attach: function (context, settings) {
    if (!$.support.positionFixed) {
      return;
    }

    $('.form-actions', context).once('formfooter', function () {
      $(this).data("drupal-formfooter", new Drupal.formFooter(this));
    });
	
	
  }
};

/**
 * Constructor for the formFooter object. Provides sticky footers/actions to forms.
 *
 * @param table
 *   DOM object to adjust with sticky.
 */
Drupal.formFooter = function (formactions) {
  var self = this;
  
  this.height = $(formactions).outerHeight();

  this.original = $(formactions);
  $(formactions).wrap('<div class="form-actions-wrapper" />');
  
  this.wrapper = this.original.closest(".form-actions-wrapper");
  this.wrapper.css("min-height", this.original.height()+"px");
  
  this.original.css("max-width", this.wrapper.width()+"px");
  
  this.form = this.original.closest("form");
  
  $(window)
    .bind('scroll.drupal-formfooter', $.proxy(this, 'eventhandlerRecalculateStickyFooter'))
    .bind('resize.drupal-formfooter', { calculateWidth: true } , $.proxy(this, 'eventhandlerRecalculateStickyFooter'));
	
	$(window).trigger('scroll.drupal-formfooter');
};

/**
 * Event handler: recalculates position of the sticky table header.
 *
 * @param event
 *   Event being triggered.
 */
Drupal.formFooter.prototype.eventhandlerRecalculateStickyFooter = function (event) {
	var self = this;
	
	this.form.addClass("sticky-boundary");
  
  var calculateWidth = event.data && event.data.calculateWidth;
  
  //window resize means the buttons might have changed height
  if(calculateWidth){
	  this.original.css("max-width", this.wrapper.width()+"px");
	  this.height = this.original.outerHeight();
	  this.wrapper.css("min-height", this.original.height()+"px");
	  
  }
  
  var form_scroll_point = this.form.offset().top;
  var top_scroll_point = this.wrapper.offset().top;
  var sticking = false;
  var window_scroll_top = $(window).scrollTop();
	var window_bottom = window_scroll_top+$(window).height();
	
	if(window_bottom > form_scroll_point+this.height*2){
	if(window_bottom < top_scroll_point+this.height){
		
			sticking = true;
		}
	}
	
	if (sticking) {
		 this.wrapper.addClass("form-actions-sticky");
	} else {
		this.wrapper.removeClass("form-actions-sticky");
	}
  
  
  
  
  
  
  
};

})(jQuery);