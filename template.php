<?php

 // Outputs site breadcrumbs with current page title appended onto trail
    function flatmin_breadcrumb($variables) {
      $breadcrumb = $variables['breadcrumb'];
      if (!empty($breadcrumb)) {
        $crumbs = '<ul class="breadcrumb">';
        $array_size = count($breadcrumb);
        $i = 0;
        while ( $i < $array_size) {
          $crumbs .= '<li class="breadcrumb-' . $i;
          if ($i == 0) {
            $crumbs .= ' first';
          }
          if ($i+1 == $array_size) {
            $crumbs .= ' last';
          }
          $crumbs .=  '">' . $breadcrumb[$i] . '</li> ';
          $i++;
        }
        $crumbs .= '<li class="active">'. drupal_get_title() .'</li></ul>';
        return $crumbs;
      }
    }


/**
 * Override or insert variables into the html template.
 */
function flatmin_preprocess_html(&$vars) {

  // Add conditional CSS for IE8 and below.
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));

  // Add conditional CSS for IE7 and below.
  drupal_add_css(path_to_theme() . '/css/ie7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));

  // Add conditional CSS for IE6.
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 6', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));

  // Add theme name to body class.
  $vars['classes_array'][] = 'adminimal-theme';

  // Add icons to the admin configuration page.
  if (theme_get_setting('display_icons_config')) {
    drupal_add_css(path_to_theme() . '/css/icons-config.css', array('group' => CSS_THEME, 'weight' => 10, 'preprocess' => FALSE));
  }
}